/**
*	Shift.cpp
*	Author: Daniel Silva
*	Data: 03/11/2017
*
*	Utilizar Registrador de Deslocamento 74hc595
*
*/

#include "Arduino.h"
#include "shiftlib.h"

Shift::Shift (int latchPin, int dataPin, int clockPin, int qtdRegistradores)
{
	pinMode(latchPin, OUTPUT);
	pinMode(dataPin, OUTPUT);
	pinMode(clockPin, OUTPUT);
	
	int _latchPin = latchPin;
	int _dataPin = dataPin; 
	int _clockPin = clockPin;
	int _qtdRegistradores = qtdRegistradores; //QUANTIDADE DE REGISTRADORES (74HC595).
}
void Shift::shiftWrite(int pin){

  int countShift = 1;
  int data = 256;
  for (int i=1; i<=pin; i++){
    if (data <= 1){
      data = 256;
      countShift++;
    }
    data /= 2;
  }
  digitalWrite(_latchPin, LOW);
  for (int i=_qtdRegistradores; i>0; i--){
    if (i == countShift){
      shiftOut(data);
    }
    else{
      shiftOut(0);
    }
  }
  digitalWrite(_latchPin, HIGH);
}

void Shift::shiftOut(byte dataOut){
  //Desloca 8 bits, com o bit menos significativo (LSB) sendo deslocado primeiro, no extremo ascendente do clock.
  boolean pinState;
  digitalWrite(_dataPin, LOW); //Deixa o registrador de deslocamento pronto para enviar dados.
  digitalWrite(_clockPin, LOW);

  for (int i=0; i<8; i++){ //Para cada bit em dataOut, envie um bit.
    digitalWrite(_clockPin, LOW); //Define clockPin como LOW antes de enviar o bit.

    //Se o valor de dataOut E(&) de uma mascara de bits forem verdadeiros, defina pinState como 1 (HIGH).
    if (dataOut & (1<<i)){
      pinState = HIGH;
    }
    else{
      pinState = LOW;
    }

    digitalWrite(_dataPin, pinState); //Define dataPin como HIGH ou LOW, dependendo de pinState.
    digitalWrite(_clockPin, HIGH); //Envia o bit no extremo ascendente do clock.
  }
  digitalWrite(_clockPin, LOW); //Interrompe o deslocamento de dados.
}
