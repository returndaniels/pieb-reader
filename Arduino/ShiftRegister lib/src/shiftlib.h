/**
*	Shift.h
*	Author: Daniel Silva
*	Data: 03/11/2017
*
*	Utilizar Registrador de Deslocamento 74hc595
*
*/

#ifndef shiftlib_h
#define shiftlib_h

#include "Arduino.h"

class Shift 
{
	public:
		Shift(int latchPin, int dataPin, int clockPin, int qtdRegistradores);
		void shiftWrite(int pin);
	private:
		boolean pinState;
		void shiftOut(byte dataOut);
		int qtdRegistradores;
		int countShift;
		int data;
		int _latchPin;
		int _dataPin; 
		int _clockPin;
		int _qtdRegistradores;
};

#endif
