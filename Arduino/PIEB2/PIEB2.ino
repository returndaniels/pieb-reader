#define led 13 
char rx = 0;    // Variavel para conter o caractere recebido
 
void setup()
{
  Serial.begin(9600);   // Define a velocidade da porta como 9600 baud
  pinMode(led, OUTPUT); // Define porta 13 como saida
  Serial.flush();       // Limpa o buffer de recepção serial
}
 
void loop()
{
  if (Serial.available() >0) // Confere se recebeu dados
  {
    rx = Serial.read();      // aramazena dado rebido em rx
    Serial.flush();          // Esvazia o buffer de recuperação serial
    if (digitalRead(led) == LOW){
       digitalWrite(led,HIGH);
    }else{
      digitalWrite(led,LOW);
    }
    /*
    if (rx=='A' || rx=='a')
    {
      if (digitalRead(led) == LOW){
        digitalWrite(led,HIGH);
      }
    }
    else if (rx=='S' || rx=='s')
    {
      if (digitalRead(led) == HIGH)
      {
          digitalWrite(led,LOW);
      }
    }*/
  }
}
