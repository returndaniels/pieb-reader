#include <stdio.h>
#include <conio.h>
#include <windows.h>

#define PORTAARDUINO "com3:"
// Forwarding
HANDLE ApriSeriale();
void MostraMenu(); 
int LeggiComando();

int main()
{
	HANDLE hComm;
 	DWORD n;
	char Risposta[512]={'\0'};
	int key;
 
 	hComm=ApriSeriale();
 	if (hComm != INVALID_HANDLE_VALUE)
	{
		MostraMenu();
		while(1)
		{
			key=LeggiComando();
			WriteFile(hComm, &key, 1,&n,NULL); // Invio il comando
			Sleep(50); // attesa per leggere il risultato del comando
			ReadFile(hComm, Risposta, 512, &n, NULL);
			if (n!=0)
			{
				Risposta[n]='\0'; // terminatore di stringa
				printf("\nRisposta da ARDUINO (%d): %s", n, Risposta);
				printf("comando da inviare a ARDUINO: ");	
			}
		}
		CloseHandle(hComm);
	}
}

/* --------------------------------------------------------------------
	Funzione che continua a leggere da tastiera finch� l'utente non 
	digita un comando digitato dall'utente
--------------------------------------------------------------------- */
int LeggiComando()
{
	int key;
	do {
		key=getch();
		if(key==27) { printf("Esc"); exit(0); }
		if ( (key=='a') || (key=='A') || (key=='s') || (key=='S') || (key=='m') || (key=='M'))
		{
			putch(key);
			return(key);
		}
	} while (1);
}

/* --------------------------------------------------------------------
	Funzione che mostra il menu dei comandi
--------------------------------------------------------------------- */
void MostraMenu()
{
	printf("Help comandi disponibili:\n");
	printf("-----------------------------\n");
	printf("A   -> Accende il LED\n");
	printf("S   -> Spegne il LED\n");
	printf("M   -> Legge lo stato del LED\n");
	printf("Esc -> Per uscire\n");
	printf("-----------------------------\n");
	printf("comando da inviare a ARDUINO: ");	
}

/* --------------------------------------------------------------------
	Funzione che apre la porta seriale
--------------------------------------------------------------------- */
HANDLE ApriSeriale()
{
	HANDLE hComm;
	hComm = CreateFile( PORTAARDUINO,  
	                    GENERIC_READ | GENERIC_WRITE, 
	                    0, 
	                    0, 
	                    OPEN_EXISTING,
	                    0,//FILE_FLAG_OVERLAPPED,
	                    0);
	if (hComm == INVALID_HANDLE_VALUE)
	{
		printf("Porta seriale %s non disponibile!",PORTAARDUINO);
		return INVALID_HANDLE_VALUE;
	}
	//SetupComm(hComm, 2, 128); // set buffer sizes
	DCB config = {0};
	if((GetCommState(hComm, &config) == 0))
	{
	    printf("Lettura configurazione fallita!.");
	    return 0;
	}
	//config.DCBlength = sizeof(config);
	config.BaudRate = CBR_9600;
	config.Parity = NOPARITY;
	// config.Parity = EVENPARITY;
	config.StopBits = ONESTOPBIT;
	config.ByteSize = DATABITS_8;
	config.fAbortOnError = TRUE;
	config.fOutX = FALSE; // XON/XOFF off for transmit
	config.fInX = FALSE; // XON/XOFF off for receive
	config.fDtrControl = DTR_CONTROL_DISABLE;
	config.fRtsControl = RTS_CONTROL_DISABLE;
	
	if (!SetCommState(hComm, &config))
	{
    	printf( "Configurazione porta seriale fallita! Errore nr.: %d\n",GetLastError());
		return(0);
	}
	// printf("Configurazione seriale corrente:\n Baud Rate %d\n Parity %d\n Byte Size %d\n Stop Bits %d\n\n", 
	//		config.BaudRate, config.Parity, config.ByteSize, config.StopBits);
	// Communication timeouts are optional
	// Tell the program not to wait for data to show up
	COMMTIMEOUTS timeouts={0};
	GetCommTimeouts(hComm, &timeouts);
	timeouts.ReadIntervalTimeout = MAXDWORD;
	timeouts.ReadTotalTimeoutMultiplier = 0;
	timeouts.ReadTotalTimeoutConstant = 0;
	timeouts.WriteTotalTimeoutMultiplier = 0;
	timeouts.WriteTotalTimeoutConstant = 0;

	if (!SetCommTimeouts(hComm, &timeouts))
    	printf( "Configurazione dei timeouts fallita! Errore nr.: %d\n",GetLastError());
	//SetCommMask(hComm,EV_RXCHAR);
	return(hComm);
}
