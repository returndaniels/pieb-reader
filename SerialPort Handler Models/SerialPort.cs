using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
 
namespace Exemplo1
{
    public partial class Form1 : Form
    {
        byte[] comando = { 0, 200, 201 };
 
        public Form1()
        {
            InitializeComponent();
        }
 
        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                Porta.Open();
            }
            catch
            {
                MessageBox.Show("Cabo desconectado","Erro");
                Close();
            }
        }
 
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                if (Porta.IsOpen)
                {
                    Porta.Close();
                }
            }
            catch
            { }
        }
 
        private void button1_Click(object sender, EventArgs e)
        {           
            try
            {
                if (button1.Text == "Off")
                {
                    Porta.Write(comando, 1, 1);
                    button1.Text = "On";
                }
                else
                {
                    Porta.Write(comando, 2, 1);
                    button1.Text = "Off";
                }
            }
            catch
            {
                MessageBox.Show("Cabo desconectado", "Erro");
                Close();
            }
        }
 
        private void hScrollBar1_Scroll(object sender, ScrollEventArgs e)
        {
            try
            {
                comando[0] = (byte)hScrollBar1.Value;
                Porta.Write(comando, 0, 1);
                Text = Convert.ToString(hScrollBar1.Value);               
            }
            catch
            {
                MessageBox.Show("Cabo desconectado", "Erro");
                Close();
            }
        }
    }
}