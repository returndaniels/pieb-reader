#include <conio.h>
#include <windows.h>
#include <iostream>
#include <fstream>
#include <cstdlib>

#define PORTAARDUINO "com3:"
// Forwarding

using namespace std;

HANDLE SerialPort();
int Comando();

int main(int argc, char *argv[])
{
	HANDLE hComm;
 	DWORD n;
	int key;
		
 	hComm=SerialPort();
 	if (hComm != INVALID_HANDLE_VALUE)
	{
		
		// Abre arquivo
		ifstream file("data/arquivo.txt", ios::in|ios::binary|ios::ate);	
		if(file.is_open()){
			file.seekg(0, ios::end);	// Vai para o final do arquivo
			int size = file.tellg();	// Retorna o indice do arquivo
			file.seekg(0, ios::beg);	// Vai pro inicio do arquivo
			char character;				// Caractere de buffer
			
			// Cria array em memoria alocada
			char * array  = (char *) malloc(size*sizeof(char));
			
			for(int i = 0; i < size; ++i){
				file.get(character);	// L� caractere
				array[i] = character;	// Igula indice [i] da lista � charactere do get()
				cout << array[i];		//	Imprime indice [i] da lista			putch(array[i]);
			//	Sleep(100);
				WriteFile(hComm, &array[i], 1,&n,NULL); // Invio il comando
				//	cout << character;		// Imprime caractere
			}
			cout << "\n";
			file.close();				// Fecha arquivo
		}	
	
		while(1)
		{
			key=Comando();
			WriteFile(hComm, &key, 1,&n,NULL); // Invio il comando
		}
		CloseHandle(hComm);
	}	
	system("PAUSE");
}

// L� COMANDO INSERIDO
int Comando()
{
	int key;
	do {
		key=getch();
		putch(key);
		if(key==27) { printf("Esc"); exit(0); }
		
		return(key);
	} while (1);
}

// ABRE PORTA SERIAL
HANDLE SerialPort()
{
	HANDLE hComm;
	hComm = CreateFile( PORTAARDUINO,  
	                    GENERIC_READ | GENERIC_WRITE, 
	                    0, 
	                    0, 
	                    OPEN_EXISTING,
	                    0,//FILE_FLAG_OVERLAPPED,
	                    0);
	if (hComm == INVALID_HANDLE_VALUE)
	{
		cout << "Porta serial " << PORTAARDUINO << " indisponivel!" << endl;
		return INVALID_HANDLE_VALUE;
	}
	//SetupComm(hComm, 2, 128); // set buffer sizes
	DCB config = {0};
	if((GetCommState(hComm, &config) == 0))
	{
	    cout << "Leitura das configura��es falhou!." << endl;
	    return 0;
	}
	//config.DCBlength = sizeof(config);
	config.BaudRate = CBR_9600;
	config.Parity = NOPARITY;
	// config.Parity = EVENPARITY;
	config.StopBits = ONESTOPBIT;
	config.ByteSize = DATABITS_8;
	config.fAbortOnError = TRUE;
	config.fOutX = FALSE; // XON/XOFF off for transmit
	config.fInX = FALSE; // XON/XOFF off for receive
	config.fDtrControl = DTR_CONTROL_DISABLE;
	config.fRtsControl = RTS_CONTROL_DISABLE;
	
	if (!SetCommState(hComm, &config))
	{
    	printf( "Configura��o da porta serial falhou! Erro nr.: %d\n",GetLastError());
		return(0);
	}
	COMMTIMEOUTS timeouts={0};
	GetCommTimeouts(hComm, &timeouts);
	timeouts.ReadIntervalTimeout = MAXDWORD;
	timeouts.ReadTotalTimeoutMultiplier = 0;
	timeouts.ReadTotalTimeoutConstant = 0;
	timeouts.WriteTotalTimeoutMultiplier = 0;
	timeouts.WriteTotalTimeoutConstant = 0;

	if (!SetCommTimeouts(hComm, &timeouts))
    	printf( "Configura��o de timeouts falhou! Erro nr.: %d\n",GetLastError());

	return(hComm);
}
